/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './src/**/*.{js,jsx,ts,tsx}',
  ],
  theme: {
    fontFamily: {
      'Coolvetica': ['Coolvetica'],
      'LaBelleAurore': ['La Belle Aurore'],
      'sans-serif': ['sans-serif'],
    },
    extend: {
      animation: {
        fadeIn: 'fadeIn 2s 1.8s backwards',
        fadeInAnimation: 'fadeInAnimation 1s 1.8s backwards',
        fadeInUp: 'fadeInUp 2s 2s backwards',
      }
    },
  },
  plugins: [],
}

